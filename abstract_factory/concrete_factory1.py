from abstract_factory import AbstractFactory
from abstract_producta import AbstractProductA
from abstract_productb import AbstractProductB
from concrete_producta1 import ConcreteProductA1
from concrete_productb1 import ConcreteProductB1


class ConcreteFactory1(AbstractFactory):
    """
    Concrete Factories produce a family of products that belong to a single
    variant. The factory guarantees that resulting products are compatible. Note
    that signatures of the Concrete Factory's methods return an abstract
    product, while inside the method a concrete product is instantiated.
    """

    def create_product_a(self) -> AbstractProductA:
        return ConcreteProductA1()

    def create_product_b(self) -> AbstractProductB:
        return ConcreteProductB1()
