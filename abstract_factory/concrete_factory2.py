from abstract_factory import AbstractFactory
from abstract_producta import AbstractProductA
from abstract_productb import AbstractProductB
from concrete_producta2 import ConcreteProductA2
from concrete_productb2 import ConcreteProductB2


class ConcreteFactory2(AbstractFactory):
    """
    Each Concrete Factory has a corresponding product variant.
    """

    def create_product_a(self) -> AbstractProductA:
        return ConcreteProductA2()

    def create_product_b(self) -> AbstractProductB:
        return ConcreteProductB2()
