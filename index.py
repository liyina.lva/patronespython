from abstract_factory.abstract_factory import AbstractFactory
from abstract_factory.concrete_factory1 import ConcreteFactory1
from abstract_factory.concrete_factory2 import ConcreteFactory2

a = 5
b = 7
c = b if b > 6 else a
print(c)
print(a + b)
print('Hello Team IPora')


def client_code(factory: AbstractFactory) -> None:
    """
    The client code works with factories and products only through abstract
    types: AbstractFactory and AbstractProduct. This lets you pass any factory
    or product subclass to the client code without breaking it.
    """
    product_a = factory.create_product_a()
    product_b = factory.create_product_b()

    print(product_a.useful_function_a())
    print(product_b.useful_function_b())


if __name__ == "__main__":
    """
    The client code can work with any concrete factory class.
    """
    print("Client: Testing client code with the first factory type:")
    client_code(ConcreteFactory1())

    print("\n branch patricia")

    print("Client: Testing the same client code with the second factory type:")
    client_code(ConcreteFactory2())
